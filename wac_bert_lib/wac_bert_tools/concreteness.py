import os
import pandas as pd
import numpy as np
from collections import defaultdict
def ds2dict(concrete_ds):
    """ Using the pickled pd.Series of tokenized concreteness scores, returns a tuple of (default dictionary instance
    of data, full score). Default value is set to zero. The bottom Rating is substracted from all scores.
    pd.Series must have the model(e.g. BERT) vocab ids as index and been tokenized with the same tokenizer as the model
    concrete_ds: concreteness Data Series object containing concreteness scores as number type
    Returns: concrteness scores default dictionary"""
    min_conc = np.floor(concrete_ds.min())
    concrete_ds = concrete_ds-min_conc
    dd=defaultdict(lambda:0)
    concreteness_dict = concrete_ds.to_dict(dd)
    return concreteness_dict
def load_input(fpath, model_checkpoint=''):
    """ Loads the data from library inputs or given pickle files
    fpath: path to tokenized data series pickle file
    model_checkpoint: check match with the index of tokenized data series
    Returns: concrteness scores default dictionary"""
    dict = pd.read_pickle(fpath)
    if dict.index.name!=model_checkpoint:
       print("WARNING: Concreteness Input may have been tokenized with a different tokenizer")
    return ds2dict(dict)