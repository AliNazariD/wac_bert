import pandas as pd
import numpy as np
from collections import defaultdict
def ds2dict(wac_ds, LAST_LYR_SIZE=768, trim_func=None):
    """ Using the pickled pd.Series of WAC vectors, returns a default dictionary instance
    of wac2vec model. Default value is set to zero vector of LAST_LYR_SIZE.
    pd.Series must have the model(e.g. BERT) vocab ids as index and corresponding vectors as values
    It truncates the vector if it is larger, pads with zeros if shorter that LAST_LYR_SIZE
    wac_ds: Data Series object of wac vectors
    LAST_LYR_SIZE: The size of last hidden layer in BERT Like model, to which WAC vectors are pad/trim
    trim_func: fuction used to trim longer WAC vetors. if None, defaults to truncating
    Returns: wac vectors default dictionary"""
    vec_len = len(wac_ds.iloc[0])
    # Adjust the size of wac_ds
    trim_func = (lambda arr: arr[:LAST_LYR_SIZE]) if not trim_func else trim_func
    if vec_len>LAST_LYR_SIZE:
        wac_ds = wac_ds.apply(trim_func)
    elif vec_len<LAST_LYR_SIZE :
        zero_len = LAST_LYR_SIZE-vec_len
        pad_zeros = lambda arr: np.pad(arr,(0,zero_len),'constant', constant_values=0)
        wac_ds = wac_ds.apply(pad_zeros)

    da=defaultdict(lambda:np.zeros(LAST_LYR_SIZE))  # default array
    return wac_ds.to_dict(da)
def load_input(fpath, model_checkpoint=''):
    """ Loads the data from library inputs or given pickle files
    fpath: path to tokenized data series pickle file
    model_checkpoint: check match with the index of tokenized data series
    Returns: WAC vectors default dictionary"""
    dict = pd.read_pickle(fpath)
    if dict.index.name!=model_checkpoint:
       print("WARNING: WAC inputs may have been tokenized with a different tokenizer")
    return ds2dict(dict)



