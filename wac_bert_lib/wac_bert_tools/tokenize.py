#!/usr/bin/env python
# coding: utf-8
__author__= "Ali Nazari"
import pandas as pd   
def tokenize_df(df, tokenizer):
    """ tokenizes a dataframe. The data frame must have two columns ['word','measure']
    'measure' is concreteness score/rating/wac vector or ... values
    It only returns words that result in a single token."""
    assert all(['word' in df.columns, 'measure' in df.columns])
    IDs = tokenizer.name_or_path
    tokenize = lambda t:list(
                tokenizer.encode_plus(
                t,
                None,
                add_special_tokens=False,
                is_split_into_words=False,
                return_tensors='np',# np or tf or pt
                return_attention_mask=False,
            ).values())[0][0]
    df[IDs] = df['word'].apply(tokenize)
    valid_df = df[df[IDs].map(len)==1]
    valid_df[IDs] = pd.DataFrame(valid_df[IDs].tolist(), valid_df.index)
    valid_df.set_index(IDs,drop=True, inplace=True)
    return valid_df['measure']

def External_Data(parser, parser_arg, tokenizer, tokenize=True, pkl=''):
    """
    Tokenizes external data to be incorporated into WAC-BERT model
    parser= function parses the file at fname, it must produce a dataframe with two columns ['word','measure']
    parser_arg= dictionary of the parser arguments 
    tokenizer= <str> name of hugging face tokenizer used/to-be-used to tokenize data
    tokenize= Boolean default to True, whether to tokenize the data or return the parser's measure column as is.
    pkl= str, path to save a pickle instance of the tokenized DataSeries e.g. './inputs/mydata.pkl', default '' does not save a pickle
    Returns= the dataseries with index of tokenizer ids and the name of tokenizer as index name
    """
    df = parser(**parser_arg)
    if tokenize:
        df = tokenize_df(df, tokenizer)
        df = df.groupby([df.index.name]).mean().to_frame()
    ds = df['measure']
    ds.index.name=tokenizer
    if pkl: ds.to_pickle(pkl, protocol=4)
    return ds