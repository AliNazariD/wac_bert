import pandas as pd
import os
from datetime import datetime
def register(**provided_arg):
    """May be called right before training step to register the propertie`s of a the model 
    and the study to keep track of stuff. if any parameter left empty at call time, it asks for inputs through terminal
    provided_arg = {
        'study_name'='<str>', default is an empty string. if empty: arg won't be added to model_registry.csv, 
        'model'= '<str>', default is an empty string, 
        'tokenizer'= '<str>' ,
        'concreteness'= '<str>', default is an empty string, 
        'wac2vec'= '<str>', default is an empty string, 
        'data'='<str>', default is an empty string, 
        'date'=	time, default is current timestamp
        'metric'= '<str>', default is an empty string, 
        'metric_value'= '<str>', default is an empty string, 
        'epoch'= '<str>', default is an empty string, 
        'max_len'= '<str>', default is an empty string, 
        'batch_Size'= '<str>', default is an empty string, 
        'purpose'= '<str>', default is an empty string, 
        'notes'='<str>', default is an empty string, 
        'fDir'= "./", default is the directory of external functions script
        }
    Returns: The final arguments and a message for successful registry and unsuccessful resgistry """
    try:
        time = datetime.now().strftime("%m/%d/%Y_%H:%M:%S")
        arg = {
            'study_name':'',
            'model': '',
            'tokenizer': '' ,
            'concreteness': '',
            'wac2vec': '',
            'data':'',
            'date':	time,
            'metric': '',
            'metric_value': '',
            'epoch': '',
            'max_len': '',
            'batch_Size': '',
            'purpose': '',
            'notes':'',
            'fDir': "./"
        }
        arg.update(provided_arg)
        inputs = {k:input("Enter %s: "%k) for k,v in arg.items() if not v and k!='metric_value'}
        arg.update(inputs)
        if arg['study_name']:  
            fpath = arg['fDir']+'/model_registry.csv'
            list_arg = {k:[v] for k,v in arg.items()}
            new_row = pd.DataFrame.from_dict(list_arg)
            if os.path.exists(fpath):
                df = pd.read_csv(fpath)
                df = df.append(new_row, ignore_index=True)
            else:
                df  = new_row.copy()
  
            df.to_csv(fpath, index=False)
        return arg
        print("Study is Registered.")
    except Exception as e:
        print("Registry was unsuccessful.")
        print(e)
    
def update_metric(value, fDir):
    """ Updates the the metric (e.g. Accuracy) for the last line of model's registry
    Overwrites the current registry at fDir"""
    fpath = fDir+"/model_registry.csv"
    reg = pd.read_csv(fpath)
    reg.loc[len(reg)-1,'metric_value']=value
    reg.to_csv(fpath, index=False)