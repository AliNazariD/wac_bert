#!/usr/bin/env python
# coding: utf-8
__author__= "Ali Nazari"
# Parsers
import pandas as pd
import pickle
def concreteness_data(data_dir):
    """ Reads the data in .csv and returns dataframe with ['word','conc'] columns.
    conc: is the concretness score out of 5, with minimum being 1"""
    dataz = pd.read_csv(data_dir, na_values=[], na_filter=False)
    concrete_df = dataz[['Word','Conc.M']]
    concrete_df.columns=['word','measure']
    return concrete_df[['word','measure']]

def wac2vec(wac_dir):
    wac2vec = pickle.load(open(wac_dir,'rb'))
    wac_df  = pd.DataFrame.from_records(zip(wac2vec.values(),wac2vec.keys()), columns=['vec','word'])
    wac_df['measure'] = wac_df['vec'].apply(lambda e:e[0])
    return wac_df[['word','measure']]

def clip_wac(fname):
    """ This dosen't require tokenizing because it is based on BERT vocabulary"""
    wac = pd.read_csv(fname, header=None, keep_default_na=False    
                                                    ,engine='python'
                                                    ,delim_whitespace=True
                                                    )
    wac.rename({0:'word'}, inplace=True, axis=1)
    wac['measure'] = wac.iloc[:,1:].values.tolist()
    wac.drop(wac.columns[1:-1], axis=1, inplace=True)
    
    return wac[['word','measure']]

def inferred_conc(fname, IDs):
    """ if the tokenized is uncased, it only tokenizes the uncased words in the data
    IDs: tokenizer.name_or_path"""
    inferred_conc = pd.read_csv(fname, delim_whitespace=True, header=0,na_values='', keep_default_na=False,engine='python' )
    inferred_conc.rename({'WORD':'word','RATING':'measure'}, axis=1, inplace=True)
    if 'uncased' in IDs:
        inferred_conc= inferred_conc[inferred_conc['word'].str.islower()]
    return inferred_conc[['word','measure']]