# README #
 
#### Quick summary
 
wac_bert_tool is a Python library that provides utilities to feed the WAC_BERT_Pipeline.ipynb with proper input data. The input data includes the wac2vec vectors and concreteness scores for English tokens.
 
* Version 0.1.0
 
### Summary of set up
Users can modify and re-create the library.
* `pip install wheel setuptools` libraries in the Python environment
* Create the library using this command `>python setup.py bdist_wheel`
* Make sure you refer to the correct directory in the main pipeline when installing the wac_bert_tool library (e.g., `!pip install ./wac_bert_lib/wac_bert_tools-0.1.0-py3-none-any.whl`)
 
### Modules
* concreteness (Contains functions to assign defaults and converting Data Series to Dictionaries)
* wac (Contains functions to assign defaults and converting Data Series to Dictionaries)
* registrar (Registers model information to a model registry)
* parser (Contains built-in parsers specific to each input data)
* tokenize (tokenize the tokens in the input data after being parsed)