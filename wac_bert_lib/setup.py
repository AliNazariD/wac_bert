from setuptools import find_packages, setup
setup(
    name='wac_bert_tools',
    packages=find_packages(),
    version='0.1.0',
    description='Library to fine tune Hugging Face BERT-like models with Word-As-Classifier (WAC) models while considering concreteness of terms',
    author='AliNazari',
    license='SLIM',
    install_requires=['pandas','numpy'],
    setup_requires=[]  #  installed automatically only when you run
    )